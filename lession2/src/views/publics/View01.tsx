import { View, Text, Button } from "react-native";
import RootNavigation from "../RootNavigation";
import { useEffect, useState } from "react";
import { useGlobalStore } from "../stores/useGlobalStore";
import axios from "axios";




const View01 = ({ navigation }: any) => {



    const { themeMode, setThemeMode } = useGlobalStore();

    const [dataView, setDataView] = useState({
        name: "Hello",
        id: 1
    })

    const [user, setUser] = useState<any>(undefined);

    const [isLoading, setIsLoading] = useState(false);


    const callApi = async () => {

        let data = JSON.stringify({
            "username": "abi-luan",
            "password": "123"
        });

        let config = {
            method: 'post',
            maxBodyLength: Infinity,
            url: 'http://swm-auth-be.danghung.xyz/api/user/login',
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        };
        setIsLoading(true)
        try {

            const response = await axios.request(config)
            setUser(response.data)

        } catch (error) {

        }
        setIsLoading(false)

    }


    // useEffect(() => {
    //     callApi();
    // }, [])



    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text style={{ fontSize: 20 }}>View01</Text>

            <Button
                title="CallAPI"
                onPress={callApi}
            />

            <Text style={{ fontSize: 20, color: "red" }}>{`Username: ${user?.username || ""}`}</Text>


            {themeMode === "light" ? (
                <Button
                    title="Change dark"
                    color={"blue"}
                    onPress={() => setThemeMode("dark")}
                />
            ) : (
                <Button
                    title="Change light"
                    color={"blue"}
                    onPress={() => setThemeMode("light")}
                />

            )}

            <Button
                title="Go to Details"
                onPress={() => navigation.navigate("View02")}
            />
        </View>
    );
}

export default View01;