

import { DrawerContentComponentProps } from "@react-navigation/drawer";
import { useNavigation } from "@react-navigation/native";
import { View, Text, Button } from "react-native";
import RootNavigation from "./RootNavigation";



const menus = [
    {
        targetView: "View01"
    },
    {
        targetView: "View02"
    },
    {
        targetView: "View03"
    },
    {
        targetView: "View04"
    },
]


const MenuView = (props: DrawerContentComponentProps) => {

    // const navigation = RootNavigation.navigationRef;
    const navigation = useNavigation<any>();
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            {menus.map(({ targetView }) => (<Button
                key={targetView}
                title={targetView}
                onPress={() => {
                    navigation.navigate(targetView)
                }}
            />))}
        </View>
    );
}

export default MenuView;